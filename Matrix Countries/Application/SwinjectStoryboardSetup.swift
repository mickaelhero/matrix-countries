//
//  SwinjectStoryboardSetup.swift
//  Matrix Countries
//
//  Created by Mickael Belhassen on 07/08/2019.
//  Copyright © 2019 Mickael Belhassen. All rights reserved.
//

import Swinject
import SwinjectStoryboard
import SwinjectAutoregistration

extension SwinjectStoryboard {
    
    static let container = defaultContainer
    
    class func setup() {
        Container.loggingFunction = nil
        registerServices(to: container)
        registerViewModels(to: container)
        registerViewControllers(to: container)
    }
    
    private class func registerServices(to container: Container) {
        container.register(APIClientService.self) { r in
            APIClient()
        }.inObjectScope(.container)

        container.register(UICoordinatorService.self) { r in
            UICoordinator()
        }.inObjectScope(.container)
    }
    
    private class func registerViewModels(to container: Container) {
        container.autoregister(CountriesListViewModelService.self, initializer: CountriesListViewModel.init).inObjectScope(.weak)
    }
    
    private class func registerViewControllers(to container: Container) {
        // Home
        container.storyboardInitCompleted(AllCountriesListViewController.self) { r, c in
            c.viewModel = r.resolve(CountriesListViewModelService.self)
        }

        container.storyboardInitCompleted(RegionCountriesViewController.self) { r, c in
            c.viewModel = r.resolve(CountriesListViewModelService.self)
        }
    }
    
}


