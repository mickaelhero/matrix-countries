//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import UIKit

class Alerts: NSObject {

    class func message(for viewController: UIViewController, title: String, message: String, closeButtonTitle: String = "Close", closeHandler: (()->())? = nil) {

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let actionClose = UIAlertAction(title: closeButtonTitle, style: .default, handler: { _ in
            closeHandler?()
        })

        alert.addAction(actionClose)

        viewController.present(alert, animated: true, completion: nil)
    }

    class func messageWithActions(for viewController: UIViewController, title: String, message: String, doneButtonTitle: String = "Done", cancelButtonTitle: String = "Cancel", doneHandler: (()->())?, cancelHandler: (()->())?) {

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let actionDone = UIAlertAction(title: doneButtonTitle, style: .default, handler: { _ in
            doneHandler?()
        })

        let actionCancel = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler:  { _ in
            cancelHandler?()
        })

        alert.addAction(actionDone)
        alert.addAction(actionCancel)

        viewController.present(alert, animated: true, completion: nil)
    }

}

