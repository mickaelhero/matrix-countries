//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import Foundation

public class MutableLiveData<T> {

    public typealias Observer = (_ observable: MutableLiveData<T>, T) -> Void
    private var observers: [Observer]

    public var value: T? {
        didSet {
            if let value = value {
                notifyObservers(value)
            }
        }
    }

    public init(_ value: T? = nil) {
        self.value = value
        observers = []
    }

    public func observeForever(observer: @escaping Observer) {
        self.observers.append(observer)
    }

    public func removeObserver(){
        self.observers.removeAll()
    }

    private func notifyObservers(_ value: T) {
        self.observers.forEach { [unowned self](observer) in
            observer(self, value)
        }
    }

}


// MARK: - Extension / Protocol

protocol MutableLiveDataProtocol {

}

extension MutableLiveDataProtocol {

    public func observe<T>(for observable: MutableLiveData<T>, with: @escaping (T) -> ()) {
        observable.observeForever { observable, value in
            DispatchQueue.main.async {
                with(value)
            }
        }
    }

}
