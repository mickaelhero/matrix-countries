//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import Foundation

struct Consts {

    struct StoryboardName {
        static let main = "Main"
    }

    struct TableViewCells {
        struct Identifiers {
            static let country = "CountryCell"
        }
    }

    struct Segues {
        struct FromAllCountriesListControllerTo {
            static let RegionCountriesController = "AllCountriesListViewController.RegionCountriesListViewController"
        }
    }

}
