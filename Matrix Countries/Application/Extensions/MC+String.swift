//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import Foundation

// MARK: - Format

extension String {

    func format(_ arguments: CVarArg...) -> String {
        let arg = arguments.map {
            if let arg = $0 as? Int { return String(arg) }
            if let arg = $0 as? Float { return String(arg) }
            if let arg = $0 as? Double { return String(arg) }
            if let arg = $0 as? Int64 { return String(arg) }
            if let arg = $0 as? String { return String(arg) }

            return "(null)"
        } as [CVarArg]

        return String.init(format: self, arguments: arg)
    }

}


// MARK: - Localization

extension String {

    var localized: String {
        return NSLocalizedString(self, comment: "")
    }

}