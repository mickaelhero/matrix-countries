//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import Foundation

struct Localizations {
    static let ascending = "ASCENDING".localized
    static let descending = "DESCENDING".localized
    static let sort = "SORT".localized
    static let by = "BY".localized
    static let error = "ERROR".localized
}
