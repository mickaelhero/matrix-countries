//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import Foundation

struct Currency: Codable {
    let code, name, symbol: String?
}
