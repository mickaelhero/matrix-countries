//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import Foundation

struct Translations: Codable {
    let de, es, fr, ja: String?
    let it: String?
    let br, pt: String
    let nl, hr: String?
    let fa: String
}