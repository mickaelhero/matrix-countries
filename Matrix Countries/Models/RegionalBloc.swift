//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import Foundation

struct RegionalBloc: Codable {
    let acronym: String
    let name: String
    let otherAcronyms: [String]
    let otherNames: [String]
}
