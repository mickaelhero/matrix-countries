//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import UIKit
import SwinjectStoryboard

protocol UICoordinatorService {
    func initialStoryboard() -> UIViewController?
}


class UICoordinator: UICoordinatorService {

    func initialStoryboard() -> UIViewController? {
        let main = SwinjectStoryboard.create(name: Consts.StoryboardName.main, bundle: nil, container: SwinjectStoryboard.container)
        let viewController: UIViewController = main.instantiateInitialViewController()!

        return viewController
    }

}
