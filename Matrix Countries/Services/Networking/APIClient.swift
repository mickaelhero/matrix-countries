//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import Foundation
import Alamofire

protocol APIClientService {
    func getCountries(completion: @escaping (_ error: Error?, _ data: [Country]?) -> ())
    func getCountries(for region: String, completion: @escaping (_ error: Error?, _ data: [Country]?) -> ())
}

class APIClient: APIClientService {

    // MARK: Creating generic GET function

    private func get<T: Codable>(route: Route, completion: @escaping (_ error: Error?, _ data: [T]?) -> ()) {
        Alamofire.request(route.path).responseJSON { response in
            guard let data = response.data else {
                completion(response.error, nil)
                return
            }

            let results: [T]? = try? JSONDecoder().decode([T].self, from: data)
            completion(response.error, results)
        }
    }

}


// MARK: - Get countries

extension APIClient {

    func getCountries(completion: @escaping (Error?, [Country]?) -> ()) {
        get(route: .getAllCountries) { (error: Error?, data: [Country]?) in
            completion(error, data)
        }
    }

    func getCountries(for region: String, completion: @escaping (Error?, [Country]?) -> ()) {
        get(route: .getCountries(region: region)) { (error: Error?, data: [Country]?) in
            completion(error, data)
        }
    }

}