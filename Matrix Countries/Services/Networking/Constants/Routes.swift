//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import Foundation

struct APIConstants {
    static let baseURL = "https://restcountries.eu/rest/v2/"
}


enum Route {
    case getAllCountries
    case getCountries(region: String)

    var path: String {
        let baseURL = "\(APIConstants.baseURL)%@"

        switch self {
            case .getAllCountries:
                return baseURL.format("all")
            case .getCountries(let region):
                return baseURL.format("region/\(region)")
        }
    }
}
