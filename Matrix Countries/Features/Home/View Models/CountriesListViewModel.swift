//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import Foundation


class CountriesListViewModel: CountriesListViewModelService {

    let apiClient: APIClientService

    var allCountries: MutableLiveData<[Country]> = MutableLiveData()
    var regionCountries: MutableLiveData<[Country]> = MutableLiveData()
    var delegate: CountriesListViewModelDelegate?


    init(apiClient: APIClientService) {
        self.apiClient = apiClient
    }

}


// MARK: - Queries

extension CountriesListViewModel {

    func getCountries() {
        apiClient.getCountries { [weak self] error, countries in
            guard let self = self else { return }

            self.delegate?.showError(error)
            self.allCountries.value = countries
        }
    }

    func getCountries(for region: String) {
        apiClient.getCountries(for: region) { [weak self] error, countries in
            guard let self = self else { return }

            self.delegate?.showError(error)
            self.regionCountries.value = countries
        }
    }

}


// MARK: - Other

extension CountriesListViewModel {

    func sortByName(isAscending: Bool) {
        if isAscending {
            allCountries.value = allCountries.value?.sorted { $0.name < $1.name }
        } else {
            allCountries.value = allCountries.value?.sorted { $0.name > $1.name }
        }
    }

    func sortByArea(isAscending: Bool) {
        if isAscending {
            allCountries.value = allCountries.value?.sorted { $0.area ?? 0 < $1.area ?? 0 }
        } else {
            allCountries.value = allCountries.value?.sorted { $0.area ?? 0 > $1.area ?? 0 }
        }
    }

}
