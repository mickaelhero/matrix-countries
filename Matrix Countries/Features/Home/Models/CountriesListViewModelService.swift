//
// Created by Mickael Belhassen on 2019-08-07.
// Copyright (c) 2019 Mickael Belhassen. All rights reserved.
//

import Foundation

protocol CountriesListViewModelService {
    func getCountries()
    func sortByName(isAscending: Bool)
    func sortByArea(isAscending: Bool)
    func getCountries(for region: String)

    var allCountries: MutableLiveData<[Country]> { get }
    var regionCountries: MutableLiveData<[Country]> { get }
    var delegate: CountriesListViewModelDelegate? { get set }
}

protocol CountriesListViewModelDelegate {
    func showError(_ error: Error?)
}