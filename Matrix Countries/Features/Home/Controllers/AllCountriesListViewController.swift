//
//  ViewController.swift
//  Matrix Countries
//
//  Created by Mickael Belhassen on 07/08/2019.
//  Copyright © 2019 Mickael Belhassen. All rights reserved.
//

import UIKit

class AllCountriesListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var viewModel: CountriesListViewModelService!


    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: IBAction

    @IBAction func sortByNameAction(_ sender: Any) {
        sortByName()
    }

    @IBAction func sortByAreaAction(_ sender: Any) {
        sortByArea()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Consts.Segues.FromAllCountriesListControllerTo.RegionCountriesController {
            guard let vc = segue.destination as? RegionCountriesViewController,
                  let country = sender as? Country
                    else { return }

            vc.country = country
        }
    }

}


// MARK: - Setup controller

extension AllCountriesListViewController {

    private func setup() {
        viewModel.delegate = self

        setupTableView()
        setObservers()
        getCountries()
    }

}


// MARK: - Networking & view model delegate methods

extension AllCountriesListViewController: CountriesListViewModelDelegate {

    private func getCountries() {
        viewModel.getCountries()
    }

    func showError(_ error: Error?) {
        guard let error = error else { return }
        Alerts.message(for: self, title: Localizations.error, message: error.localizedDescription)
    }

}


// MARK: IBAction

extension AllCountriesListViewController {

    private func sortByName() {
        displaySortSheet(ascending: {
            self.viewModel.sortByName(isAscending: true)
        }, descending: {
            self.viewModel.sortByName(isAscending: false)
        })
    }

    private func sortByArea() {
        displaySortSheet(ascending: {
            self.viewModel.sortByArea(isAscending: true)
        }, descending: {
            self.viewModel.sortByArea(isAscending: false)
        })
    }

    private func displaySortSheet(ascending: @escaping () -> (), descending: @escaping () -> ()) {
        let alert = UIAlertController(title: Localizations.sort, message: Localizations.by, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: Localizations.ascending, style: .default, handler: { (UIAlertAction) in
            ascending()
        }))

        alert.addAction(UIAlertAction(title: Localizations.descending, style: .default, handler: { (UIAlertAction) in
            descending()
        }))

        self.present(alert, animated: true)
    }

}


// MARK: - TableView and delegate methods

extension AllCountriesListViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.allCountries.value?.count ?? 0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let country = viewModel.allCountries.value?[indexPath.row] else { return }
        performSegue(withIdentifier: Consts.Segues.FromAllCountriesListControllerTo.RegionCountriesController, sender: country)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Consts.TableViewCells.Identifiers.country, for: indexPath)

        guard let country = viewModel.allCountries.value?[indexPath.row] else { return cell }

        cell.textLabel?.text = country.nativeName
        cell.detailTextLabel?.text = country.name

        return cell
    }

    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }

}


// MARK: - Observers

extension AllCountriesListViewController {

    private func setObservers() {
        observeCountries()
    }

    private func observeCountries() {
        viewModel.allCountries.observeForever { [weak self] (_: MutableLiveData<[Country]>, _: [Country]) in
            guard let self = self else { return }
            self.tableView.reloadData()
        }
    }

}
