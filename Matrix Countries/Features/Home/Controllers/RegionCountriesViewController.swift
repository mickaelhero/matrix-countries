//
//  RegionCountriesViewController.swift
//  Matrix Countries
//
//  Created by Mickael Belhassen on 07/08/2019.
//  Copyright © 2019 Mickael Belhassen. All rights reserved.
//

import UIKit

class RegionCountriesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: CountriesListViewModelService!
    var country: Country?


    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

}


// MARK: - Setup controller

extension RegionCountriesViewController {

    private func setup() {
        viewModel.delegate = self

        setupTableView()
        setObservers()
        getCountries()
    }

}


// MARK: - Networking & view model delegate methods

extension RegionCountriesViewController: CountriesListViewModelDelegate {

    private func getCountries() {
        guard let region = country?.region else { return }
        viewModel.getCountries(for: region)
    }

    func showError(_ error: Error?) {
        guard let error = error else { return }
        Alerts.message(for: self, title: Localizations.error, message: error.localizedDescription)
    }

}


// MARK: - TableView and delegate methods

extension RegionCountriesViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.regionCountries.value?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Consts.TableViewCells.Identifiers.country, for: indexPath)

        guard let country = viewModel.regionCountries.value?[indexPath.row] else { return cell }

        cell.textLabel?.text = country.nativeName
        cell.detailTextLabel?.text = country.name

        return cell
    }

    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }

}


// MARK: - Observers

extension RegionCountriesViewController {

    private func setObservers() {
        observeCountries()
    }

    private func observeCountries() {
        viewModel.regionCountries.observeForever { [weak self] (_: MutableLiveData<[Country]>, _: [Country]) in
            guard let self = self else { return }
            self.tableView.reloadData()
        }
    }

}
